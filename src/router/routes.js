
export default [
  {
    path: '/',
    component: () => import('layouts/HomeTemplate'),
    children: [{ path: 'home', component: () => import('pages/home') },
                { path: 'login', component: () => import('pages/login') },
                 { path: 'callback', component: () => import('pages/callback') }]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
