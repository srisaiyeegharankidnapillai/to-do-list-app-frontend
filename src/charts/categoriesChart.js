import {Doughnut, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
	extends: Doughnut,
	mixins: [reactiveProp],
	props: ['chartData', 'options'],
	mounted: function()
	{
		console.log("Rendering Categories Level Chart");
		this.renderChart(this.chartData, this.options);
	}
}