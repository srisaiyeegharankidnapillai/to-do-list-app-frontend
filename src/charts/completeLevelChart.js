import {Doughnut, mixins } from 'vue-chartjs'

export default {
	extends: Doughnut,
	mixins: [mixins.reactiveProp],
	props: ['chartData', 'options'],
	 watch: {
    'chartData': {
      handler (newOption, oldOption) {
      	console.log("into Watcher");
        this._chart.destroy();
        this.renderChart(this.chartData, this.options)
      },
      deep: true
    }
},
	mounted: function()
	{
		//1st digit
		console.log("Rendering Complete Level Chart");
		console.log (this.options);
		
		this.renderChart(this.chartData, this.options);
	}

}